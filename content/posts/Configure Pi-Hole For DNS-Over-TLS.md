---
title: "Configure Pi Hole for DNS Over TLS"
date: 2018-08-02T22:22:11-07:00
draft: false
---

Protect your DNS traffic from snooping with DoT  
------
---

<br>
<a href="https://pi-hole.net"><img class="centered" src="https://static.bartonbytes.com/img/dot/pihole_logo.png" alt="Pi-hole Logo" height="100" width = "100"></a>

Pi-hole is a wonderful program for both technical and non-technical users to run a local DNS caching server, allowing you to block malicious and ad-serving domains.
One of the fundamental flaws of DNS is the lack of encryption or integrity, which allows your ISP to snoop DNS traffic or spoof a DNS response.
DNS-over-TLS will not completely solve these problems (see the end of this tutorial), but it provides a step in the right direction. Let's get started.

Pi-hole uses a fork of dnsmasq as it's DNS server.  To use DoT, we will actually need to run an additional DNS server, 
Unbound, that provides this feature. (If you want to use CoreDNS instead, check out [my other guide](https://bartonbytes.com/posts/how-to-configure-coredns-for-dns-over-tls/))
To install on a Debian-based system, run the following:

`sudo apt install -y unbound dnsutils`

Once installed, run the following command to grab a configuration file:

`sudo wget https://bartonbytes.com/pihole.txt -O /etc/unbound/conf.d/pihole.conf`

Here's [a link to the file](../../pihole.txt), and a copy of the contents here:

```
## DNS Over TLS, Simple ENCRYPTED recursive caching DNS, TCP port 853  
## unbound.conf -- original at https://calomel.org/unbound\_dns.html  
## tweaks by bartonbytes.com  
server:  
access-control: 127.0.0.0/8 allow  
cache-max-ttl: 14400  
cache-min-ttl: 600  
do-tcp: yes  
hide-identity: yes  
hide-version: yes  
interface: 127.0.0.1  
minimal-responses: yes  
prefetch: yes  
qname-minimisation: yes  
rrset-roundrobin: yes  
ssl-upstream: yes  
use-caps-for-id: yes  
verbosity: 1  
port: 5533  
#  
forward-zone:  
name: "."  
forward-addr: 9.9.9.9@853         # quad9.net primary  
forward-addr: 1.1.1.1@853         # cloudflare primary  
forward-addr: 149.112.112.112@853 # quad9.net secondary  
forward-addr: 1.0.0.1@853         # cloudflare secondary
```

You'll notice that this DNS server is configured to be accessible only on the local machine.  It will open up port 5533.  The config file includes the
Quad9 and Cloudflare upstream DNS servers, which you can change or add to if necessary.  

Make sure that Unbound is running:  

`sudo systemctl restart unbound && sudo systemctl enable unbound`  

To test that Unbound can fulfill your DNS requests, run the following `dig` command:  

`dig @127.0.0.1 example.com -p 5533`  

Now, we need to tell Pi-hole's dnsmasq to use this local port as it's upstream DNS server. In the GUI, go to Settings -> DNS, 
and set a custom IPv4 server with the value `127.0.0.1#5533`  

<img class="centered" src="https://static.bartonbytes.com/img/dot/dns_setting.png" alt="DNS Setting">

Now we must restart Pi-hole:  

`sudo systemctl restart pihole-FTL`  

... and voila! The upstream DNS requests sent from your Pi-hole will be encrypted using TLS.  

As mentioned earlier, DNS-over-TLS is not a perfect solution to your privacy concerns.  No matter how you protect your DNS traffic,
the name of the websites that you visit will still be visible in the SNI of your HTTPS traffic, allowing your ISP (and any other intermediary)
to view it.  DoT somewhat protects integrity by preventing intermediaries from manipulating your DNS requests or their responses.  However,
you are still trusting the upstream DNS server- in our case, Quad9 and Cloudflare- to provide the correct responses.  

Another option to secure DNS traffic is DNS-over-HTTPS. I chose DoT because the `cloudflared` program would not work on my Raspberry Pi 1 Model B+.  DoH has the advantage of being harder to block or detect, because the DNS traffic is encapsulated inside of HTTPS traffic destined for port 443.  This is also a slight disadvantage due to the additional traffic overhead of the HTTPS headers, which makes DoH somewhat slower than DoT.


