---
title: "How to Fix Aruba Clearpass Virtual Machine Losing IP Address on Restart"
date: 2023-10-12T10:52:05-07:00
draft: false
---

Resolve Aruba Clearpass 10.8 on Hyper-V Losing It's IP Address After Being Shut Down
------
---

We were running Aruba Clearpass version 10.8 as a Hyper-V virtual machine when we encountered the following problem: after the VM was shut down or restarted, the Clearpass server would lose it's IP address, and it would have to be manually re-added.

This caused some serious issues when it came to syncing, because for some reason, our Clearpass subscriber couldn't connect to the publisher, and so would refuse to add the IP address. This would necessitate rolling the subscriber back to a previous snapshot in order to restore the IP address.

I found some [community support threads](https://community.arubanetworks.com/discussion/clearpass-hyper-v-loses-mgmt-ip-after-reboot) about the issue, but the suggested fixes weren't working. Finally I figured out that the suggested steps had to be done in a very specific order.

Here are the steps I took to fix the issue of Aruba Clearpass losing it's management IP address:

1. Drop subscribers from the publisher

2. Shut down Clearpass virtual machine

3. Delete the Hyper-V network interfaces on the virtual machine and re-add them

4. Start the VM and configure the management IP address

5. Shut down the VM and change the network interfaces from **Dynamic** to **Static** under "Advanced Features"

6. Start the VM and configure the management IP address

6. Run the following command: `system network-refresh`

7. Restart the VM one last time

After doing the tasks in this specific order, our Aruba Clearpass virtual machines stopped losing their IP address on shutdown.