---
title: "Create a Free Website With Github Pages and Hugo"
date: 2019-07-26T16:52:25-07:00
draft: false
---

A quick guide to writing and hosting a website for $0 in cost
------
---

  
Making your own website may seem like a lost art, but it's easier than ever to create a free, no-strings-attached 
website.

We'll be using the Hugo static site generator, which allows us to write Markdown that gets translated into HTML/CSS.  Our 
generated HTML/CSS get stored into a git repository on Github.  [Github Pages](https://pages.github.com/) will make this 
repository available as a fully-fledged website, complete with HTTPS and the option to add a custom domain name.  We'll go 
through the process step-by-step.

#### Installing Hugo and Starting Our Project

I'll be assuming that you're using Debian GNU/Linux. Everything should be able to be done on Linux, Windows, and OS X, but 
specific commands and configurations will vary.

To begin, install Hugo server from your distro's repositories:

`sudo apt install hugo -y`

You can also download the binaries, which are likely a more recent version, from 
the [Hugo Github repo](https://github.com/gohugoio/hugo/releases).

Navigate to the directory where we want to store our project, and then run this command:

`hugo new site yoursitename && cd yoursitename`

<img class="centered" src="https://static.bartonbytes.com/img/hugosite/gen_site.png" alt="Hugo new site command">

Now, we need to add a theme. Let's go with the [Hyde-Y theme](https://github.com/enten/hyde-y) from Enten.  Navigate into 
the `themes` directory of your project, and then clone the theme into this directory, using the following commands:

`cd themes && git clone https://github.com/enten/hyde-y.git`

Now, we need to edit our config.toml file to tell it our domain name and which theme to use. Here's what mine looks like:

<img class="centered" src="https://static.bartonbytes.com/img/hugosite/themetoml.png" alt=".toml config">

At this point, we can check out what our site look like.  From our project's root directory, run `hugo server`.  Open up 
a web browser and go to `http://localhost:1313`

<img class="centered" src="https://static.bartonbytes.com/img/hugosite/site_1.png" alt="A site that doesn't look quite right">

Hmm, that doesn't look quite right. Each Hugo theme is slightly different, and this one expects files at both `data/Menu.toml`
 and `data/FootMenu.toml`, as well as some extra settings in `config.toml`.  Assuming we don't cancel `hugo server` while 
 we edit and save those files, the web page in our browser will automatically update with our changes.
 
<img class="centered" src="https://static.bartonbytes.com/img/hugosite/data_menu.png" alt="Required settings">  

<img class="centered" src="https://static.bartonbytes.com/img/hugosite/site_2.png" alt="A site that looks okay">
 
Much better!  Let's add one post before we share our website with the world. From the project root, run 
 
`mkdir content/post && hugo new content/post/"Welcome to My New Blog.md"`
 
Add whatever text you'd like, and make sure to switch `Draft: true` to `Draft: false`. Here's what my post looks like:  

<img class="centered" src="https://static.bartonbytes.com/img/hugosite/post_markdown.png" alt="The markdown for a post">  
<img class="centered" src="https://static.bartonbytes.com/img/hugosite/post.png" alt="A Hugo blog post">

If you've been checking out your site while running `hugo server`, you may have noticed that your links, like About, lead 
 to a '404 not found' message. We can create this page in the same way we made the post, by running:
 
`hugo new about.md`
 
The About page is created under content/. Like with our post, make sure to switch it to `Draft: false`.  
 
#### Github Pages ####
 
Awesome! We're done making our website, but no one can see it until we deploy using Github pages. Obviously, you want a 
Github account.  On the Github website, create a repository named `your_github_username.github.io`. Clone this repo into 
the root of our Hugo site. Then, run the following:
 
 `hugo -d your_github_username.github.io`
 
 cd into that same repo, then commit & push.  
 
 After that, then... just kidding! That's it; you're done. Wait a few minutes, and your website will be online at 
 http://your_github_username.github.io . From here, you can enable HTTPS, add a custom domain name, and make new posts on 
 your blog!
 
