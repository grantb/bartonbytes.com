---
title: "About"
date: 2018-08-01T21:12:40-07:00
draft: false
---

I am Grant Barton, an IT professional in Washington state.  I am particularly interested in Linux, server administration, and user-friendly security.

Occasionally, I will find a lack of tutorials, guides, documentation, or any other resources when attempting to complete a specific task.  This blog is my attempt to correct these deficiencies, and provide guides that I believe others will find useful.


[Linode affiliate link](https://www.linode.com/?r=2719dcb8fdfe9ffca1962f9d07f13d5297a4383d)  

</br>

---
This blog uses the One theme for Hugo by author [resugary](https://github.com/resugary/hugo-theme-one).
